# Debian 10 (Buster) Ansible Test Image

Debian 10 (Buster) Docker container for Ansible playbook and role testing.

## Author

Modified and maintained by *Fabricio Boreli*  
Originally created in 2017 by [Jeff Geerling](https://www.jeffgeerling.com/).
